#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 by Joey Hess.
#
# This version is for a hypothetical package that builds an
# architecture-dependant package, as well as an architecture-independent
# package.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This is the debhelper compatability version to use.
#export DH_COMPAT=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

PACKAGE=mrtg
TMP=`pwd`/debian/mrtg

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp:
	dh_testdir

	./configure --prefix=/usr --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info --sysconfdir=/etc
	$(MAKE)

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp

	[ ! -f Makefile ] || $(MAKE) clean
	rm -f Makefile config.log config.status
	rm -f bin/rateup.o # not (always?) cleaned by make clean
	rm -f config.h

	dh_clean -Xrej -Xorig

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	dh_installdocs
	dh_installman
	dh_install

   # Manual install :(

	cp -a lib/mrtg2/*.pm $(TMP)/usr/share/perl5
	# Remove the four SNMP_Session modules which are now in libsnmp-session-perl
	rm -f $(TMP)/usr/share/perl5/BER.pm
	rm -f $(TMP)/usr/share/perl5/SNMP_Session.pm
	rm -f $(TMP)/usr/share/perl5/SNMP_util.pm
	rm -f $(TMP)/usr/share/perl5/Net_SNMP_util.pm
	install -m 644 debian/cron.d $(TMP)/etc/cron.d/mrtg
	install -m 755 bin/cfgmaker $(TMP)/usr/bin
	install -m 755 bin/indexmaker $(TMP)/usr/bin
	install -m 755 bin/mrtg $(TMP)/usr/bin
	install -m 755 bin/rateup $(TMP)/usr/bin
	install -m 644 debian/sample-mrtg.cfg $(TMP)/etc/mrtg.cfg
	#dh_installdocs doc/*.html doc/*.txt doc/*.pod README ANNOUNCE
	#dh_installexamples -p$(PACKAGE)-contrib contrib/* debian/exim-stats

	mkdir -p $(TMP)/usr/share/doc/mrtg/examples
	cp -a contrib/* $(TMP)/usr/share/doc/mrtg/examples/
	cp -a debian/exim-stats $(TMP)/usr/share/doc/mrtg/examples/
	rm -rf $(TMP)/usr/share/doc/mrtg/examples/xlsummary  # Remove the bit that doens't run on Debian anyway
	# Remove the extra COPYING files in some contrib files
	rm $(TMP)/usr/share/doc/mrtg/examples/GetSNMPLinesUP/COPYING || true
	rm $(TMP)/usr/share/doc/mrtg/examples/mrtg-blast/COPYING || true

	# Fix some #! lines
	find $(TMP)/usr/share/doc/mrtg/examples -type f | xargs -r \
		perl -i -pe '$$_ = "#!/usr/bin/perl\n" if m|^#!.*[/\\]perl|;'
	# Correcting deps
	find $(TMP)/usr/share/doc/mrtg/examples -type f | xargs -r chmod 0644
	find $(TMP)/usr/share/doc/mrtg/examples -type f | xargs -r -n 1 \
		sh -c "if head -1 $$1 | grep -q '^#!'; then echo $$1; fi" | \
		xargs -r chmod 0755

	# logrotate
	cp -a debian/logrotate $(TMP)/etc/logrotate.d/mrtg

	mv ${TMP}/usr/share/doc/mrtg/examples/* ${TMP}/../mrtg-contrib/usr/share/doc/mrtg/examples

	# Clean empty dir
	rm -rf $(TMP)/usr/share/doc/mrtg/examples

# Build architecture-independent files here.
# Pass -i to all debhelper commands in this target to reduce clutter.
binary-indep: DH_OPTIONS=-i
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_installdocs
	dh_installexamples
	dh_installmenu
#	dh_installemacsen
#	dh_installpam
#	dh_installinit
	dh_installcron
#	dh_installmanpages
	dh_installinfo
#	dh_undocumented
	dh_installchangelogs
	dh_link
	dh_compress -Xexamples/
	dh_fixperms
	# You may want to make some executables suid here.
#	dh_suidregister
	dh_installdeb
#	dh_perl
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
# Pass -a to all debhelper commands in this target to reduce clutter.
binary-arch: DH_OPTIONS=-a
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_installdocs
	dh_installexamples
	dh_installmenu
#	dh_installemacsen
#	dh_installpam
#	dh_installinit
	dh_installcron
#	dh_installmanpages
	dh_installinfo
#	dh_undocumented
	dh_installchangelogs
	dh_strip
	dh_link
	dh_compress
	dh_fixperms
	# You may want to make some executables suid here.
#	dh_suidregister
	dh_installdeb
#	dh_makeshlibs
	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
