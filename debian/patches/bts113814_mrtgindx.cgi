commit 759fe33b999a863639231d28d680b64d2ae6bf5f
Author: Sandro Tosi <morph@debian.org>
Date:   Tue Jun 3 03:07:27 2008 +0200

    applied patch from Ryan Murray <rmurray@cyberhqz.com>; Closes: #113814

Index: mrtg/contrib/mrtgindex.cgi/mrtgindx.cgi
===================================================================
--- mrtg.orig/contrib/mrtgindex.cgi/mrtgindx.cgi	2011-08-14 13:41:56.893213708 +0200
+++ mrtg/contrib/mrtgindex.cgi/mrtgindx.cgi	2011-08-14 13:42:09.677614948 +0200
@@ -71,11 +71,11 @@
 
 #-------------------------------------------------------
 # Modify this statement to match your configuration
-@config_files = ('mrtg.cfg'); # Single config file
+@config_files = ('/etc/mrtg.cfg'); # Single config file
 
 #@config_files = ('mrtg.cfg', 'mrtg-ping.cfg'); # Two config files
 #@config_files = ('mrtg.cfg', 'yahoo.ping', 'netscape.ping', 'msn..ping', 
-                   'att.ping'); # anal retentive
+#                 'att.ping'); # anal retentive
 
 #-------------------------------------------------------
 
@@ -89,6 +89,7 @@
     open(In, $cfg = shift @config_files) ||
 	die "Can't open $cfg. Check \@config_files array.\n";
     while(<In>){
+	chdir($1) if /^WorkDir:\s*(.+)$/;
 	next unless /^Title\[(.*)\]:\s*(.+)$/; # Look for a title keyword
 
 	$router = lc $1;
@@ -115,7 +116,7 @@
     table({-width=>"100\%"}, TR(
 #-------------------------------------------------------
     # Uncomment the following line if you have Count.cgi installed.
-    td({-align=>left, width=>"25\%"}, img({-src=>"/cgi-bin/Count.cgi?display=clock"})),
+    #td({-align=>left, width=>"25\%"}, img({-src=>"/cgi-bin/Count.cgi?display=clock"})),
 #-------------------------------------------------------
 
     td("Click graph for more info")));
